---
title: "Meet the Team"
subtitle: ""
# meta description
description: "Meet the Team"
draft: false
team_members:
  - name: Constanza Paulina Silva Gallardo
    image_webp:
    image: images/team/constanza_s-round.png
    content: Ph.D. in Behavioral Health from the Pennsylvania State University. She is an Assistant Professor at the Health and Society Research Center (CISS) at the Universidad Mayor in Santiago, Chile. Constanza's areas of expertise are genomics, nicotine, and development from an interdisciplinary perspective. In addition, she has begun to work on bioethical issues in genomics and Indigenous groups. She is part of the Mapochogasta Diaguita Community and the trans-Andean network of Diaguita women. For more information, visit [here](linktr.ee/ConstanzaPSilva)
  - name: Constanza de la Fuente Castro
    image_webp:
    image: images/team/constanza_dlf-round.png
    content: Postdoctoral scholar at the University of Chicago, Department of Human Genetics. Constanza previously completed a Ph.D. at the Center for GeoGenetics at the University of Copenhagen, Denmark, where she applied molecular and bioinformatics tools to study past populations in Siberia and the Americas.
  - name: Tomás González Zarzar
    image_webp:
    image: images/team/tomas-round.png
    content: Ph.D. in Biological Anthropology and Bioethics at Pennsylvania State University. Currently, Tomás is an Assistant Professor at the Society and Health Research Center and Director of the Social Sciences and Arts Nucleus at Universidad Mayor. His research interests include multi-omics integration using the genome, exposome, and metabolome to interrogate complex disease outcomes and sex differences.
  - name: Nicolás Montalva
    image_webp:
    image: images/team/nicolas-round.png
    content: Ph.D. in Anthropology at UCL. Assistant Professor at Society and Health Research Center, Universidad Mayor, Chile. My research interests are centered on the study of the effects of subsistence systems and social structures in human biology. I have been focused on rural communities and pastoralism.
  - name: Ayelén Tonko-Huenucoy
    image_webp:
    image: images/team/ayelen-round.png
    content: Biological anthropologist at Jetarkte Spa and external researcher at the Natural Museum of Natural History in Santiago, Chile. Member of the Kawésqar community in Puerto Edén, Chile. She has experience in archaeological salvage and as a research assistant. Currently working in the rescue of the language and culture of the Kawésqar people.
  - name: Felipe Martínez
    image_webp:
    image: images/team/felipe-round.png
    content: Ph.D. in Biological Anthropology, University of Cambridge. Associate Professor at the Escuela de Antropología, Pontificia Universidad Católica de Chile. My research interests are related to human evolution and diversity, from molecular genetics to morphology and behavior. Since 2012, I have been a research associate at the Center for Intercultural and Indigenous Research (CIIR), studying archaeological heritage, identity, and repatriation. In 2016, I joined the Paleo-Primate Project at Gorongosa National Park, Mozambique, to conduct primatological and paleontological fieldwork in the southernmost part of the Great Rift Valley.
  - name: Maanasa Raghavan
    image_webp:
    image: images/team/maanasa-round.png
    content: Assistant Professor in the Department of Human Genetics at the University of Chicago heading an ancient genomics lab focused on human evolution. Maanasa completed her Ph.D. and postdoctoral research in ancient genomics from the University of Copenhagen and the University of Cambridge, respectively, and has worked on the genetic histories of Indigenous peoples in the Americas and, more recently, South Asian populations.
---
