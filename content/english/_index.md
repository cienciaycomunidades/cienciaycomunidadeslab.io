---
####################### Banner #########################
banner:
  title : "Science and communities"
  image : "images/banner.png"
  content : "We want to contribute to the discussion and improvement of the ethical standards of genetic research with Indigenous peoples in Chile."
  button:
    enable : false
    label : ""
    link : ""

##################### Event ##########################
event:
  enable : true
  event_item:
    - title: "Join Us!"
      image: "images/events/Taller2023.png"
      content: "Join us in the second Science and Communities workshop, *Genomics and Identities*. The workshop will occur from November 28 to December 2 at the Villarrica Campus, Pontificia Universidad Católica, Villarrica, Chile."
      button:
        enable: true
        label: "Apply here"
        link: "events/workshop-02"

##################### Descripciones ##########################
service:
  enable : true
  service_item:
    # service item loop
    - title : "General description"
      images:
      - ""
      content : "The **Science and Indigenous Communities Study Group** is composed of a group of Chilean, Indigenous, and international researchers, working in human genetics, biological anthropology, bioarcheology, health, and ethics. Our objective is to **contribute to the discussion and improvement of the ethical standards of genetic research with Indigenous peoples**, through the improvement of genetic literacy embedded in our educational and communication projects. In addition, we want our study group to **serve as a platform to promote equitable collaborations between Indigenous communities and researchers that support the sovereignty of the communities.**"
      button:
        enable : true
        label : "About us"
        link : "about"
    # service item loop
    - title : "Acknowledgements"
      images:
      - ""
      content : "This project has been funded by the University of Chicago through the Provost's Global Faculty Awards: Latin America 2021-2022 in collaboration with faculty of the same institution."
      button:
        enable : false
        label : ""
        link : ""

######################### News #####################
news:
  enable: true
  title: "News"
  item_show: 2

######################### Team #####################
team:
  enable : true
  title : Our team
  team_member :
    # team member loop
    - name : Constanza Silva Gallardo
      image_webp : 
      image : images/team/constanza_s-round.png
      designation : 
      content : Assistant Professor at the Health and Society Research Center (CISS) at the Universidad Mayor in Santiago, Chile. Member of the Mapochogasta Diaguita Community and the trans-Andean network of Diaguita women.
      social :
        - icon : ti-twitter-alt # themify icon pack : https://themify.me/themify-icons
          link : "#"
    
    # team member loop
    - name : Constanza de la Fuente
      image_webp : 
      image : images/team/constanza_dlf-round.png
      designation : 
      content : Postdoctoral scholar in the Department of Human Genetics at the University of Chicago, USA.
      social :
        - icon : ti-twitter-alt # themify icon pack : https://themify.me/themify-icons
          link : "#"

    - name : Tomás González Zarzar
      image_webp : 
      image : images/team/tomas-round.png
      designation : 
      content : Assistant Professor at the Society and Health Research Center (CISS) and Director of the Social Sciences and Arts Nucleus at Universidad Mayor in Santiago, Chile.
      social :
        - icon : ti-twitter-alt # themify icon pack : https://themify.me/themify-icons
          link : "#"

     # team member loop
    - name : Nicolás Montalva
      image_webp : 
      image : images/team/nicolas-round.png
      designation : 
      content : Assistant Professor at Society and Health Research Center, Universidad Mayor, Chile.
      social :
        - icon : ti-twitter-alt # themify icon pack : https://themify.me/themify-icons
          link : "#"

    - name : Ayelén Tonko-Huenucoy 
      image_webp : 
      image : images/team/ayelen-round.png
      designation : 
      content : Biological anthropologist at Jetarkte Spa and external researcher at the Natural Museum of Natural History in Santiago, Chile. Member of the Kawésqar community in Puerto Edén, Chile.
      social :
        - icon : ti-twitter-alt # themify icon pack : https://themify.me/themify-icons
          link : "#"

    - name : Felipe Martínez 
      image_webp : 
      image : images/team/felipe-round.png
      designation : 
      content : Associate Professor at the Escuela de Antropología, Pontificia Universidad Católica de Chile.
      social :
        - icon : ti-twitter-alt # themify icon pack : https://themify.me/themify-icons
          link : "#"

    - name : Maanasa Raghavan
      image_webp : 
      image : images/team/maanasa-round.png
      designation : 
      content : Assistant Professor in the Department of Human Genetics at the University of Chicago heading an ancient genomics lab.
      social :
        - icon : ti-twitter-alt # themify icon pack : https://themify.me/themify-icons
          link : "#"
---
