---
title: "Second workshop: Genomics and identities"
date: 2023-08-09
image: "images/events/Taller2023.png"
type: "featured"
description: "Genomics and identities"
draft: false
---

The ***Genomics and identities*** workshop is a 4-day workshop whose objective is to facilitate and promote spaces for dialogue and learning about genetic research with Indigenous Peoples in Chile.

### The main objectives of the workshop are:

- Contextualize current practices of genomic research in Chile with Indigenous Peoples in the international context of human rights and interpretations derived from these studies.
- Equip Indigenous Peoples and communities with knowledge and tools on genomic research in Chile, where they play a central role in the consent and approval of these projects in their communities.
- Facilitate discussions regarding ways to build equitable relationships between researchers and Indigenous Peoples communities.

### Place and date:

This workshop will take place between **November 28 and December 2, 2023** at the [Villarrica Campus](https://www.uc.cl/universidad/nuestros-campus/villarrica/) of the Pontificia Universidad Católica.