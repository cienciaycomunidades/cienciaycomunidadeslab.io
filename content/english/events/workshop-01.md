---
title: "Workshop: Genomics and identities in Chile"
date: 2022-03-28
image: "images/events/workshop2022.png"
type: "regular"
description: "Genomics and identities in Chile"
draft: false
---

The ***Genomics and identities in Chile*** workshop is a 3-day workshop whose objective is to facilitate and promote spaces for dialogue and learning about genetic research with Indigenous Peoples in Chile.

### The main objectives of the workshop are:

- Contextualize current practices of genomic research in Chile with Indigenous Peoples in the international context of human rights and interpretations derived from these studies.
- Equip Indigenous Peoples and communities with knowledge and tools on genomic research in Chile, where they play a central role in the consent and approval of these projects in their communities.
- Facilitate discussions regarding ways to build equitable relationships between researchers and Indigenous Peoples communities.

### Place and date:

This workshop will take place between **June 2 and 4, 2022** at the San Joaquín Campus of the Pontificia Universidad Católica in Santiago, Chile.
