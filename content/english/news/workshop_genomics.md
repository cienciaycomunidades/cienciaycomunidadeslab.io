---
title: "Workshop: Genomcis and identities in Chile"
date: 2022-06-07
draft: false
image: "images/events/taller2022.jpeg"
---

On June 2, 3 and 4, the **Genomics and Identities in Chile** workshop was held at the Pontificia Universidad Católica in Santiago.
Fifteen participants from the Aymara, Colla, Diaguita, Chango, Kawésqar, Mapuche, Mapuche-Huilliche, Mapuche-Pehuenche and Rapa Nui peoples were received to discuss genetic research with Indigenous Peoples in Chile.
The discussion sessions ranged from basic aspects of genetics and human variation, international experiences in working with indigenous populations, to ethical aspects and regulations of scientific research.
