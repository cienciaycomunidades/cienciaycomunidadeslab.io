---
title: "Science and communities mentioned in Science journal"
date: 2022-06-07
draft: false
image: "https://www.science.org/do/10.1126/science.add3751/full/_20220607_nid_chileindigenous_loncon_gettyimages-1233805307-1675045566407.jpg"
lang: EN
---

Science magazine recently published the article [*Indigenous peoples of Chile seek fairer partnerships with scientists*](https://www.science.org/content/article/chiles-indigenous-peoples-seek-fairer-partnerships-with-scientists) where the Science and Communities group is mentioned.

In the article, Constanza de la Fuente, a member of Science and Communities, establishes that it is necessary to "approach the communities not only saying, ‘This is an informed consent form, sign it and give me your sample,’ but trying to generate a dialogue with them."
