---
title: "The Articulation of Genomics, Mestizaje, and Indigenous Identities in Chile: A Case Study of the Social Implications of Genomic Research in Light of Current Research Practices"
date: "2022-03-03"
doi: https://doi.org/10/gpk753
authors: ["Silva, C. P.", "de la Fuente Castro, C.", "González Zarzar, T.", "Raghavan, M.", "Tonko-Huenucoy, A.", "Martínez, F. I.", "Montalva, N."]
journal: Frontiers in Genetics
---