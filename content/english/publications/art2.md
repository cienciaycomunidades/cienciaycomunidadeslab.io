---
title: "Recommendations for Sustainable Ancient DNA Research in the Global South: Voices From a New Generation of Paleogenomicists"
date: "2022-04-26"
doi: https://doi.org/10.3389/fgene.2022.880170
authors: ["Ávila-Arcos, M. C.", "de la Fuente Castro, C.", "Nieves-Colón, M. A.", "Raghavan, M."]
journal: Frontiers in Genetics
---