---
title: "About us"
subtitle: ""
# meta description
description: "About us"
draft: false
---

We are a team of Chilean, Indigenous, and international researchers,  working in human genetics, biological anthropology, bioarchaeology, health, and ethics. We became aware of the common misuse of genetic information and its interpretation through our work in academic institutions and museums and our involvement with Indigenous communities. This situation affects historically marginalized groups. Thus, the mission of our **Science and Indigenous Communities Study Group is to contribute to the discussion and improvement of the ethical standards for research involving Indigenous populations in Chilean territory**. In line with our mission, our objective is to improve genetic literacy by providing an educational and communicative platform for Indigenous peoples to discuss human genetics, its benefits, limitations, and correct interpretation. In addition, we aim to **promote equitable collaborations between Indigenous communities and researchers that support the sovereignty of the communities**. For this, our study group will serve as a platform to build long-term relationships between academia and Indigenous researchers, representatives, and communities that serve to train and support community members.

# Our goals

Our work seeks to generate spaces for discussion and collaboration that will help improve research standards in Chile. We aim to **promote equitable and fair relationships between Indigenous communities and researchers under the principles of mutual benefit, reciprocity, collaboration, participation, training, and transparency** (Claw et al., 2018). Our first project - scheduled for May 2022 - aims to address the problem of genetics and Indigenous identities in Chile. This workshop is designed for Indigenous community representatives on Chilean territory. The University of Chicago has funded this project through the Provost's Global Faculty Awards: Latin America 2021-2022, collaborating with academics from the same institution. This workshop has been inspired by the work of the Summer Internship for Indigenous Peoples in Genomics (SING) in the United States and Canada.

# Our vision

Research for us carries socio-political responsibilities. In our case, this means **working towards equitable and collaborative relationships between academia and Indigenous peoples that follow high ethical standards under the values of equity, justice, and reciprocity**. Therefore, our vision is to contribute to constructing a country that recognizes plurinationality by respecting Indigenous people's sovereignty, particularly in research settings. 
