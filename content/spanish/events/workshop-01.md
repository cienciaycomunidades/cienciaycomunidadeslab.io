---
title: "Primer taller: Genómica e identidades en Chile"
date: 2022-03-28
image: "images/events/workshop2022.png"
type: "regular"
description: "Genómica e identidades en Chile"
draft: false
---

El taller ***Genómica e identidades en Chile*** es un taller de 3 días cuyo objetivo es facilitar y promover espacios de diálogo y aprendizaje sobre la investigación genética con Pueblos Originarios en Chile.

### Los principales objetivos del taller son:

- Contextualizar prácticas actuales de investigación genómica en Chile con Pueblos Originarios en el contexto internacional de derechos humanos e interpretaciones derivadas de estos estudios.
- Equipar a personas indígenas y comunidades con conocimientos y herramientas sobre la investigación genómica en Chile, donde ellos juegan un rol central en el consentimiento y aprobación de estos proyectos en sus comunidades.
- Facilitar discusiones respecto a formas de construir relaciones equitativas entre investigadores y comunidades pertenecientes a Pueblos Originarios.

### Publico objetivo y requisitos:

Este taller está dirigido principalmente a personas pertenecientes a Pueblos Originarios en Chile. Los requisitos para participar son:

- Completar la ficha de **[postulación](https://forms.gle/3m7DrDtAMyvSbGE77)**.
- Ser perteneciente a algún Pueblo Originario.
- Comprometerse a asistir los tres días del taller en su totalidad.

### Lugar y fecha:

Este taller se llevará a cabo los días **2, 3 y 4 de junio de 2022** en el Campus San Joaquín de la Pontificia Universidad Católica, en Santiago de Chile.

### Costos y beneficios:

No existe costo alguno para participar en el taller. Sin embargo, los cupos serán limitados a 15 participantes ya que la participación en el taller cubre los costos[^1] de transporte hacia/desde Santiago (en caso de residir fuera de Santiago), y alojamiento y alimentación durante los días del taller.

### Breve descripción:

El taller consistirá en charlas y sesiones de discusión grupales sobre las siguientes temáticas:
- Conceptos básicos de genética.
- ¿Qué es la ancestría genética? ¿Cómo se interpreta y cuál es su relación con la identidad?.
- Ciclo de vida de una investigación en Chile: contextualizar y describir el proceso de una investigación científica desde su formulación,consentimientos informados y publicación.
- Desafíos éticos que tiene hoy la investigación genómica respecto a su relación con las comunidades indígenas y cuál es el rol de los pueblos indígenas en estas investigaciones.

### Postulaciones

Estaremos recibiendo postulaciones en el siguiente **[formulario](https://forms.gle/3m7DrDtAMyvSbGE77)** hasta el día 20 de abril. Los seleccionados serán contactados vía correo electrónico la última semana de abril 2022.

[^1]: Estos costos serán cancelados directamente por la organización del taller y no a través de reembolsos.
