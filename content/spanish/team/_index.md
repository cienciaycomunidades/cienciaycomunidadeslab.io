---
title: "Nuestro equipo"
subtitle: ""
# meta description
description: "Conoce a nuestro equipo"
draft: false
team_members:
  - name: Constanza Paulina Silva Gallardo
    image_webp: 
    image: images/team/constanza_s-round.png
    content: Doctora en Salud y Biocomportamiento de la Universidad Estatal de Pensilvania. Actualmente es profesora asistente en el Centro de Investigación Sociedad y Salud (CISS) de la Universidad Mayor en Santiago, Chile. Las áreas de expertise de Constanza son la genómica, nicotina, desarrollo, desde una perspectiva interdisciplinaria. Además, ha comenzado a trabajar en temáticas bioéticas respecto a la genómica y grupos Indígenas. Ella es parte de la Comunidad Autónoma Diaguita Mapochogasta y de la Red Trasandina de mujeres Diaguita. Para más información visita [aqui](https://linktr.ee/ConstanzaPSilva).
  - name: Constanza de la Fuente Castro
    image_webp: 
    image: images/team/constanza_dlf-round.png
    content: Investigadora Postdoctoral en la Universidad de Chicago, Departamento de Genética Humana. Constanza realizó su doctorado en el Center for GeoGenetics de la Universidad de Copenhagen, Dinamarca, donde se especializó en la implementación de herramientas moleculares y bioinformáticas para el estudio de poblaciones pasadas en Siberia y las Américas.
  - name: Tomás González Zarzar
    image_webp: 
    image: images/team/tomas-round.png
    content: Doctor en Antropología Biológica y Bioética de la Universidad Estatal de Pensilvania. Actualmente es profesor asistente del Centro de Investigación Sociedad y Salud (CISS) y director de Núcleo de Ciencias Sociales y Artes en la Universidad Mayor. Sus áreas de interes son la integración multi-omica del genoma, exposoma, y metaboloma, para modelar enfermedades complejas e identificar diferencias entre sexos/géneros.
  - name: Nicolás Montalva
    image_webp: 
    image: images/team/nicolas-round.png
    content: Doctor en Antropología en UCL. Profesor Asistente en el Centro de Investigación en Sociedad y Salud, Universidad Mayor, Chile. Mis intereses de investigación se centran en el estudio de los efectos de los sistemas de subsistencia y las estructuras sociales en la biología. Me he especializado en comunidades rurales y pastoralismo.
  - name: Ayelén Tonko-Huenucoy
    image_webp: 
    image: images/team/ayelen-round.png
    content: Antropóloga física en Jetarkte Spa e investigadora externa en el Museo Nacional de Historia Natural. Miembro de la comunidad Kawésqar residente en Puerto Edén. Experiencia en rescate arqueológico y asistencia en investigaciones. Actualmente trabajando en el rescate de la lengua y cultura del pueblo Kawésqar.
  - name: Felipe Martínez
    image_webp: 
    image: images/team/felipe-round.png
    content: Doctor en Antropología Biológica, Universidad de Cambridge. Profesor Asociado en la Escuela de Antropología UC. Mi principal tema de interés es el estudio de la evolución y diversidad humana, desde la genética molecular hasta la morfología y el comportamiento. Desde 2012, soy investigador asociado en el Centro de Estudios Interculturales e Indígenas (CIIR) estudiando el patrimonio arqueológico, la identidad y la repatriación. Desde 2016, soy parte del Proyecto Paleo-Primate en el Parque Nacional de Gorongosa, Mozambique, donde realizamos trabajo de campo primatológico y paleontológico en la parte meridional del Gran Valle del Rift.
  - name: Maanasa Raghavan
    image_webp: 
    image: images/team/maanasa-round.png
    content: Profesora Asistente del Departamento de Genética Humana de la Universidad de Chicago, directora del Laboratorio en genómica antigua, centrado en evolución humana. Maanasa realizó su PhD e investigación postdoctoral en genómica antigua en la Universidad de Copenhagen y la Universidad de Cambridge, respectivamente. Su trabajo se ha enfocado en la reconstrucción de la historia genética de grupos Indígenas de América y, más recientemente, poblaciones del sur de Asia.
---
