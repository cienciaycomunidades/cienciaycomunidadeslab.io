---
title: "Quiénes somos"
subtitle: ""
# meta description
description: "Quiénes somos"
draft: false
---

Somos un equipo de investigadores chilenos e Indígenas, y colaboradores internacionales, que principalmente se desarrollan en las áreas de genética humana, antropología biológica, bioarqueología, salud, y ética. Mediante nuestro trabajo en instituciones académicas y museos, así como nuestra participación con comunidades indígenas, nos dimos cuenta del uso comúnmente indebido de la información genética y su interpretación; situación que afecta a grupos históricamente marginados. 
Conformamos el Grupo de Estudios de Ciencia y Comunidades Originarias con **la misión de contribuir a la discusión y mejoramiento de los estándares éticos con los cuales se realizan las investigaciones genéticas con poblaciones originarias en Chile**. De esta forma, nuestro objetivo es mejorar y promover el conocimiento sobre genética humana, sus beneficios, limitaciones y correcta interpretación, proporcionando una plataforma educativa y comunicativa para personas pertenecientes a Pueblos Originarios. Además, queremos incentivar colaboraciones equitativas entre comunidades indígenas e investigadores que apoyen la soberanía de las comunidades. Para esto, planeamos proporcionar una plataforma para construir relaciones a largo plazo entre la academia y los investigadores, representantes y comunidades indígenas, la cual sirva para capacitar y apoyar a los miembros de la comunidad en entornos de investigación.

# Qué hacemos

Nuestro trabajo busca **generar espacios de reflexión y colaboración  que ayuden a  mejorar los estándares de investigación de Chile, así como la vinculación equitativa y justa entre comunidades indígenas e investigadores bajo los principios de mutuo beneficio, reciprocidad, colaboración, participación, capacitación, y transparencia** (Claw et al., 2018).
Nuestro primer proyecto - a realizarse en el mes de Mayo del 2022 - tiene los objetivos de  abordar la problemática respecto a la genética e identidades indígenas en Chile. Este taller está dirigido a representantes de comunidades originarias del territorio Chileno. El proyecto ha sido financiado por la Universidad de Chicago a través del Provost's Global Faculty Awards: Latin America 2021-2022 en colaboración con académicos de la misma institución. Este taller ha estado inspirado por el trabajo del Summer Internship for Indigenous Peoples in Genomics (SING) de EE.UU. y Canadá.

# Nuestra visión

La investigación para nosotros conlleva responsabilidades sociopolíticas. Esto significa **trabajar por relaciones equitativas y colaborativas entre la academia y los pueblos originarios, las cuales se apeguen a altos estándares éticos bajo los valores de equidad, justicia y reciprocidad**. Por lo tanto, nuestra visión es contribuir a la construcción de un país que reconozca la plurinacionalidad respetando la soberanía y derechos consuetudinarios de los pueblos indígenas, particularmente en contextos de investigación.
