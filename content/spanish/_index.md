---
####################### Banner #########################
banner:
  title : "Ciencia y Comunidades"
  image : "images/banner.png"
  content : "Queremos contribuir a la discusión y mejoramiento de los estándares éticos con los cuales se realizan las investigaciones genéticas con poblaciones originarias en Chile"
  button:
    enable : false
    label : ""
    link : ""

##################### Event ##########################
event:
  enable : true
  event_item:
    - title: "Participa!"
      image: "images/events/Taller2023.png"
      content: "Participa en el segundo taller de Ciencia y Comunidades, *Genómica e identidades en Chile*. El taller se llevará a cabo desde el 28 de noviembre al 2 de diciembre en el Campus Villarrica, Pontificia Universidad Católica."
      button:
        enable: true
        label: "Postula acá"
        link: "events/workshop-02"

######################### Service #####################
service:
  enable : true
  service_item:
    # service item loop
    - title : "Quiénes somos"
      images: 
      - ""
      content : "El **Grupo de Estudios de Ciencia y Comunidades Originarias** está conformado por un grupo de investigadores chilenos, Indígenas y colaboradores internacionales, los cuales se desarrollan en las áreas de genética humana, antropología biológica, bioarqueología, salud, y ética. **Nuestro objetivo es contribuir a la discusión y mejoramiento de los estándares éticos con los cuales se realizan las investigaciones genéticas con poblaciones originarias en Chile**. Esto a través de proyectos educativos y comunicativos que mejoren el conocimiento de la población respecto a genética. Además, nuestro grupo de estudio servirá como plataforma para promover colaboraciones equitativas entre comunidades indígenas e investigadores de forma que apoyen la soberanía de las comunidades."
      button:
        enable : true
        label : "Conócenos"
        link : "about"
    # service item loop
    - title : "Agradecimientos"
      images:
      - ""
      content : "Este proyecto ha sido financiado por la Universidad de Chicago a través del Provost's Global Faculty Awards: Latin America 2021-2022 en colaboración con académicos de la misma institución."
      button:
        enable : false
        label : ""
        link : ""

######################### News #####################
news:
  enable: true
  title: "Noticias"
  item_show: 2

######################### Team #####################
team:
  enable : true
  title : Nuestro equipo
  team_member :
    # team member loop
    - name : Constanza Silva Gallardo
      image_webp : 
      image : images/team/constanza_s-round.png
      designation : 
      content : Profesora asistente en el Centro de Investigación Sociedad y Salud (CISS) de la Universidad Mayor en Santiago, Chile. Miembro de la Comunidad Autónoma Diaguita Mapochogasta y de la Red Trasandina de mujeres Diaguita.
      social :
        - icon : "fab fa-twitter" # themify icon pack : https://themify.me/themify-icons
          link : "https://twitter.com/ConstanzaPSilva"
    
    # team member loop
    - name : Constanza de la Fuente
      image_webp : 
      image : images/team/constanza_dlf-round.png
      designation : 
      content : Investigadora Postdoctoral en el departamento de genética humana en la Universidad de Chicago, USA.
      social :
        - icon : ti-twitter-alt # themify icon pack : https://themify.me/themify-icons
          link : "#"

    - name : Tomás González Zarzar
      image_webp : 
      image : images/team/tomas-round.png
      designation : 
      content : Profesor asistente del Centro de Investigación Sociedad y Salud (CISS) y director de Núcleo de Ciencias Sociales y Artes en la Universidad Mayor en Santiago, Chile.
      social :
        - icon : ti-twitter-alt # themify icon pack : https://themify.me/themify-icons
          link : "#"

     # team member loop
    - name : Nicolás Montalva
      image_webp : 
      image : images/team/nicolas-round.png
      designation : 
      content : Profesor Asistente en el Centro de Investigación en Sociedad y Salud, Universidad Mayor, Chile.
      social :
        - icon : ti-twitter-alt # themify icon pack : https://themify.me/themify-icons
          link : "#"

    - name : Ayelén Tonko-Huenucoy 
      image_webp : 
      image : images/team/ayelen-round.png
      designation : 
      content : Antropóloga física en Jetarkte Spa e investigadora externa en el Museo Nacional de Historia Natural de Santiago, Chile. Miembro de la comunidad Kawésqar residente en Puerto Edén.
      social :
        - icon : ti-twitter-alt # themify icon pack : https://themify.me/themify-icons
          link : "#"

    - name : Felipe Martínez 
      image_webp : 
      image : images/team/felipe-round.png
      designation : 
      content : Profesor Asociado en la Escuela de Antropología de la Pontificia Universidad Católica de Chile.
      social :
        - icon : ti-twitter-alt # themify icon pack : https://themify.me/themify-icons
          link : "#"

    - name : Maanasa Raghavan
      image_webp : 
      image : images/team/maanasa-round.png
      designation : 
      content : Profesora Asistente del Departamento de Genética Humana de la Universidad de Chicago, directora del Laboratorio en genómica antigua.
      social :
        - icon : ti-twitter-alt # themify icon pack : https://themify.me/themify-icons
          link : "#"
---