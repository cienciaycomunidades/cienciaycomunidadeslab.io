---
title: "Grupo CyC mencionado en revista Science"
date: 2022-06-07
draft: false
image: "https://www.science.org/do/10.1126/science.add3751/full/_20220607_nid_chileindigenous_loncon_gettyimages-1233805307-1675045566407.jpg"
lang: ES
description: "La revista Science publicó recientemente el artículo"
---

La revista Science publicó recientemente el artículo [*Chile’s Indigenous peoples seek fairer partnerships with scientists*](https://www.science.org/content/article/chiles-indigenous-peoples-seek-fairer-partnerships-with-scientists) en donde se menciona al grupo Ciencia y Comunidades.

En el artículo, Constanza de la Fuente, miembro de Ciencia y Comunidades, estableció que es necesario "Acercarse a las comunidades no solo diciendo: ‘Este es un consentimiento informado, fírmelo y deme su muestra’, si no tratando de generar un diálogo con ellas".
