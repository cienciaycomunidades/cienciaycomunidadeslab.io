---
title: "Taller: Genómica e identidades en Chile"
date: 2022-06-07
draft: false
image: "images/events/taller2022.jpeg"
lang: ES
---

Los días 2, 3 y 4 de junio se llevó a cabo el Taller de **Genómica e identidades en Chile** en la Pontificia Universidad Católica en Santiago.
Quince participantes de los pueblos Aymara, Colla, Diaguita, Chango, Kawésqar, Mapuche, Mapuche-Huilliche, Mapuche-Pehuenche y Rapa Nui, fueron recibidos para dialogar sobre la investigación genética con Pueblos Originarios en Chile.
Las sesiones de discusión pasaron desde aspectos básicos de genética y variación humana, experiencias internacionales en trabajos con poblaciones indígenas, hasta aspectos éticos y regulaciones de la investigación científica.

