---
title: "Pace and space in the practice of aDNA research: Concerns from the periphery"
date: "2022-12-30"
doi: https://doi.org/10.1002/ajpa.24683
authors: ["Yáñez, B.", "Fuentes, A.", "Silva, C. P.", "Figueiro, G.", "Menéndez, L. P.", "García-Deister, V.", "de la Fuente-Castro, C.", "González-Duarte, C.", "Tamburrini, C.", "Argüelles, J. M."]
journal: American Journal of Biological Anthropology
---
